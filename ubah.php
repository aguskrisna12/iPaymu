<?php 
require 'functions.php';
//ambil id
$id = $_GET["id"];
$data = query("SELECT * FROM tb_test WHERE id = $id")[0];


// jika tombol submit di tekan
if( isset($_POST["submit"]) ){
    if ( ubah($_POST) > 0 ){
        echo "
        <script> 
            alert('Data berhasil diubah!')
            document.location.href = 'index.php';
        </script>";
    } else {
        echo "
        <script> 
            alert('Data gagal diubah!')
            document.location.href = 'index.php';
        </script>";
    }
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="Stylesheet.css">
     <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
</head>
<body class="bg-primary">
    <div class="login">
        <h1 class="text-center">Insert Data</h1>
        <form action="" method="post">
        <input type="hidden" name="id" value="<?= $id; ?>">
        <div class="form-group">
            <label class="form-label" for="nama">Nama</label>
            <input class="form-control" type="text" name="nama" id="nama" required value="<?=$data["nama"];?>">
        </div>
        <div class="form-group">
            <label class="form-label" for="pekerjaan">Pekerjaan</label>
            <input class="form-control" type="text" name="pekerjaan" id="pekerjaan" required value="<?= $data["pekerjaan"]; ?>">
        </div>
        <div class="form-group">
            <label class="form-label" for="birth">Tanggal Lahir</label>
            <input class="form-control" type="date" name="birth" id="birth" required value="<?= $data["birth"]; ?>">
        </div>  
            <button class="btn btn-success w-100" type="submit" name="submit">Tambah data!</button>
        </form>
    </div>
</body>
</html>