<?php 
require "functions.php";

$data = query("SELECT * FROM tb_test ORDER BY birth ASC");

if (isset($_POST["cari"]) ){
    $data = cari($_POST["keyword"]);
    echo "<a href='index.php'>Kembali ke halaman tabel</a>";
}

// for($i = 0 ; $i < 10 ; $i++){
//     $data2["nama"] = "krisna";
//     $data2["pekerjaan"] = "akuntansi";
//     $data2["birth"] = "1996-03-11";
//     tambah($data2);
// }


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iPaymu</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <style>
        nav{
            box-shadow: 0px 5px 9px #777
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.php"><h3>iPaymu</a></h3>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="tambah.php" target="_blank">Insert Data</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.php">Edit Data</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.php">Delete Data</a>
                    </li>
                </ul>
            <form action="" method="post" class="d-flex">
                <input class="form-control me-2" type="text" name="keyword" autocomplete="off" autofocus placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-primary" type="submit" name="cari">Search</button>
            </form>
            </div>
        </div>
    </nav>
    <br>
    <table class="table table-striped mt-2">
        <tr>
            <td style ="font-weight: bold" class="text-center">No.</td>
            <td style ="font-weight: bold" class="text-center">Aksi</td>
            <td style ="font-weight: bold" class="text-center">Nama</td>
            <td style ="font-weight: bold" class="text-center">Pekerjaan</td>
            <td style ="font-weight: bold" class="text-center">Tanggal Lahir</td>
        </tr>

        <?php $i=1; ?>
        <?php foreach( $data as $dataSql ) :?>
        <tr>
            <td class="text-center"><?= $i; ?></td>
            <td class="text-center">
                <a href="ubah.php?id=<?= $dataSql["id"] ?>">Edit |</a>
                <a href="hapus.php?id=<?= $dataSql["id"] ?>" onclick="return confirm('Benar ingin menghapus ?')">Delete</a>
            </td>
            <td class="text-center"><?= $dataSql["nama"]; ?></td>
            <td class="text-center"><?= $dataSql["pekerjaan"]; ?></td>
            <td class="text-center"><?= $dataSql["birth"]; ?></td>
        </tr>
        <?php $i++ ?>
        <?php endforeach; ?>
    </table>
</body>
</html>